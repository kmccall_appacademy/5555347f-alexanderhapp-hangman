class Hangman
  attr_reader :guesser, :referee, :board, :guessed

  def initialize(options = {})
    defaults = {
      referee: ComputerPlayer.new,
      guesser: HumanPlayer.new
    }

    options = defaults.merge(options)

    @referee = options[:referee]
    @guesser = options[:guesser]
    @guessed = []
  end

  def setup
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = Array.new(length)
  end

  def take_turn
    guess = @guesser.guess(board)
    indicies = @referee.check_guess(guess)
    @guesser.guesses -= 1 if indicies.empty?
    update_board(guess, indicies)
    @guessed << guess unless @board.include?(guess)
    @guesser.handle_response(guess, indicies)
  end

  def update_board(guess, indicies)
    indicies.each { |idx| @board[idx] = guess }
  end

  def display_board
    display_board = @board.map { |el| el.nil? ? "_" : el }
    puts "\nSecret Word: #{display_board.join(' ')}"
  end

  def display_guessed
    already_guessed = @guessed.join(" ")
    puts "\nAlready guessed: #{already_guessed}" unless @guessed.empty?
  end

  def play
    setup
    display_board
    until game_over?
      take_turn
      display_board
      display_guessed
    end
    conclude
  end

  def conclude
    won? ? @guesser.won : @guesser.lost(@referee.secret_word)
  end

  def lost?
    @guesser.guesses == 0
  end

  def won?
    @board.all?
  end

  def game_over?
    won? || @guesser.guesses == 0
  end
end

class HumanPlayer
  attr_accessor :guesses
  attr_reader :dictionary, :secret_word, :secret_length

  def initialize(dictionary = File.readlines("lib/dictionary.txt").map(&:chomp))
    @dictionary = dictionary
    @guesses = 10
  end

  def pick_secret_word
    print "\nEnter the length of the secret word: "
    gets.chomp.to_i
  end

  def guess(board)
    print "\nGuess a letter: "
    gets.chomp
  end

  def handle_response(guess, indicies)
    puts "\nNope! You have #{@guesses} guesses left." if indicies.empty?
  end

  def register_secret_length(length)
    @secret_length = length
  end

  def check_guess(guess)
    puts "\nGuess: #{guess} "
    print "\nType index if guess is good, enter if not. "
    gets.chomp.split(",").map(&:to_i)
  end

  def won
    puts "\nCongratulations! You won!"
  end

  def lost(secret_word)
    puts "\nYou lose! The secret_word was #{secret_word}"
  end
end

class ComputerPlayer
  attr_accessor :guesses
  attr_reader :dictionary, :secret_word, :candidate_words

  def initialize(dictionary = File.readlines("lib/dictionary.txt").map(&:chomp))
    @dictionary = dictionary
    @guesses = 10
  end

  def candidate_words
    @cadidate_words = dictionary
  end

  def pick_secret_word
    @secret_word = @dictionary[rand(@dictionary.length)]
    secret_word.length
  end

  def check_guess(guess)
    indicies = []
    secret_word.each_char.with_index { |chr, i| indicies << i if guess == chr }
    indicies
  end

  def register_secret_length(length)
    candidate_words.select! { |word| word.length == length }
  end

  def handle_response(guess, indicies)
    puts "\n#{@guesses} guesses left." if indicies.empty?
    candidate_words.select! do |word|
      indicies.map { |idx| guess == word[idx] }.all? &&
      word.count(guess) == indicies.count
    end
  end

  def guess(board)
    frequency_array = get_frequency_array.reject do |word|
      word.map { |chr| board.include?(chr) }.any?
    end
    frequency_array[0].first
  end

  def won
    puts "\nYou lose! Better luck next time!"
  end

  def lost(secret_word)
    puts "\nYou win!#{secret_word}"
  end

  private

  def get_letter_counts
    letter_counts = Hash.new(0)
    self.candidate_words.join.split("").each { |chr| letter_counts[chr] += 1 }
    letter_counts
  end

  def get_frequency_array
    get_letter_counts.sort_by { |k, v| v }.reverse
  end
end

if __FILE__ == $PROGRAM_NAME

  # game = Hangman.new
  game = Hangman.new(referee: HumanPlayer.new, guesser: ComputerPlayer.new)

  game.play
end
